# -*- coding: utf-8 -*-

import argparse
import sys
import json
import gspread
import time
import httplib
import xml
from oauth2client.client import SignedJwtAssertionCredentials as sjac
import requests
from bs4 import BeautifulSoup
import random
from dateutil import parser as dateparser
from datetime import datetime
import urllib2
import logging

from random_ua import get_ua

API_KEY = ''
SONGKICK_URL = 'http://www.songkick.com'
MAX_RETRIES = 5

logger = logging.getLogger('songkick_manager')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
fh = logging.FileHandler('songkick_manager.log')
ch.setLevel(logging.DEBUG)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)


def is_valid_file(parser, arg):
    try:
        f = open(arg, 'r')
        f.close()
    except IOError:
        parser.error('no such file %s' % (arg,))

    return arg

def get_festival_ids(upcoming=False):
    festival_ids = []
    response = ''
    keywords = 'festival'
    if upcoming:
        next_page_url = 'http://www.songkick.com/search?page=1&per_page=10&query=%s&type=upcoming' % (keywords)
    else:
        next_page_url = 'http://www.songkick.com/search?page=1&per_page=10&query=%s&type=past' % (keywords)
    # next_page_url = 'http://www.songkick.com/search?page=1&per_page=10&query=%s' % (keywords)
    retries = 0
    while next_page_url:
        try:
            response = requests.get(next_page_url, headers={'User-Agent': get_ua()}, timeout=15)
            response.close()
        # except requests.exceptions.ConnectionError:
        except Exception as e:
            # print 'ConnectionError'
            logger.warning(str(e))
            # print e

        # req = urllib2.Request(next_page_url, headers={'User-Agent': get_ua()})
        # try:
        #     r = urllib2.urlopen(req)
        #     response = r.read()
        #     r.close()
        # except:
        #     response = ''

        if response:
            if response.status_code == requests.codes.ok:
            # if response:
                html = response.text
                # html = response
                html_tree = BeautifulSoup(html, 'lxml')
                festival_items = html_tree.findAll('li', {'class': 'festival-instance'})
                for item in festival_items:
                    if item.findAll('input', {'name': 'subject_id'}):
                        festival_id = item.findAll('input', {'name': 'subject_id'})[0]['value']
                    else:
                        festival_id = ''
                    # if item.findAll('time'):
                    #     start_date = item.findAll('time')[0]['datetime']
                    # else:
                    #     start_date = ''

                    # festival_ids.append({'festival_id': festival_id, 'start_date': start_date})
                    festival_ids.append(festival_id)

                # if not filter(None, [None if 'next_page' not in x['class'] else x for x in html_tree.findAll('span', {'class': ['disabled']})]):
                if html_tree.findAll('div', {'class': 'pagination'}):
                    if html_tree.findAll('div', {'class': 'pagination'})[0].findAll('a', {'rel': 'next'}):
                        # if html_tree.findAll('a', {'class': 'next_page'}):
                            next_page_url = SONGKICK_URL + html_tree.findAll('div', {'class': 'pagination'})[0].findAll('a', {'rel': 'next'})[0]['href']
                            # print next_page_url
                            logger.info('Scraping: ' + next_page_url)
                            time.sleep(1 * random.uniform(1, 3))
                    else:
                        next_page_url = ''

                        # with open('last.html', 'w') as outfile:
                        #     outfile.write(response.text)
                        logger.info('Scraping: no next page')
                        # print 'no next page'
                else:
                    # print 'RETRY %d' % (retries)
                    logger.info('Scraping: retry %d' % (retries))
                    retries += 1
                    time.sleep(1 * random.uniform(1, 5))

            else:
                # print response.status_code
                # print 'RETRY %d' % (retries)
                logger.info('Scraping: retry %d' % (retries))
                retries += 1
                time.sleep(1 * random.uniform(1, 5))

            if retries > MAX_RETRIES:
                retries = 0
                logger.info('Scraping: max retries reached, skipping')
                break

    return festival_ids

def get_festival_info(festival_id):
    festival_info = {}
    lineup = []
    response = ''
    retries = 0

    while not response:
        url = 'http://api.songkick.com/api/3.0/events/%s.json?apikey=%s' % (festival_id, API_KEY)
        try:
            response = requests.get(url, timeout=15)
            response.close()
        # except requests.exceptions.ConnectionError:
        except KeyboardInterrupt:
            raise
        except requests.exceptions.RequestException as e:
        # except Exception as e:
            # print 'ConnectionError'
            logger.warning(str(e))
            # print e

            retries += 1
            time.sleep(1 * random.uniform(2, 5))
            if retries >= MAX_RETRIES:
                logger.info('API calls: max retries reached, skipping')
                break
    # try:
    #     r = urllib2.urlopen(url)
    #     response = r.read()
    #     r.close()
    # except:
    #     response = ''

    if response:
        if response.status_code == requests.codes.ok:
        # if response:
            json_results = json.loads(response.text)
            # json_results = json.loads(response)
            if 'error' not in json_results['resultsPage']:
                json_results = json_results['resultsPage']['results']['event']
                festival_info['festival_name'] =json_results['displayName'] if 'displayName' in json_results else None

                start_date = json_results['start']['date'] if 'start' in json_results and 'date' in json_results['start'] else None
                end_date = json_results['end']['date'] if 'end' in json_results and 'date' in json_results['end'] else None

                if start_date:
                    start_date = dateparser.parse(start_date)
                    festival_info['start_date'] = start_date.strftime('%d/%m/%Y')
                    festival_info['year'] = start_date.year

                if end_date:
                    end_date = dateparser.parse(end_date)
                    festival_info['end_date'] = end_date.strftime('%d/%m/%Y')

                # festival_info['start_date'] =json_results['start']['date'] if 'start' in json_results and 'date' in json_results['start'] else None
                # festival_info['end_date'] =json_results['end']['date'] if 'end' in json_results and 'date' in json_results['end'] else None

                if 'venue' in json_results:
                    if 'city' in json_results['venue']:
                        json_city_info = json_results['venue']['city']
                        festival_info['city'] = json_city_info['displayName'] if 'displayName' in json_city_info else None
                        festival_info['country'] = json_city_info['country']['displayName'] if 'country' in json_city_info \
                            and 'displayName' in json_city_info['country'] else None
                    elif 'metroArea' in json_results['venue']:
                        json_city_info = json_results['venue']['metroArea']
                        festival_info['city'] = json_city_info['displayName'] if 'displayName' in json_city_info else None
                        festival_info['country'] = json_city_info['country']['displayName'] if 'country' in json_city_info \
                            and 'displayName' in json_city_info['country'] else None
                if 'performance' in json_results:
                    for item in json_results['performance']:
                        if 'displayName' in item:
                            lineup.append(item['displayName'])
                        elif 'artist' in item and 'displayName' in item['artist']:
                            lineup.append(item['artist']['displayName'])

                    headliners = lineup[:8]
                    random.shuffle(headliners)



                    festival_info['headliners'] = headliners

                    random.shuffle(lineup)
                    festival_info['lineup'] = lineup

            # else:
            #     with open('errors.log', 'a+') as outfile:
            #         outfile.write(str(festival_id))
    # pprint(json_results)
    # pprint(festival_info)
    # pprint(', '.join(lineup))

    return festival_info

def upload_json_to_google(data_filename, cred_filename, tablekey):
    json_key = json.load(open(cred_filename))
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
    gc = gspread.authorize(credentials)

    table = gc.open_by_key(tablekey)
    wks_title = 'festivals'
    cells_titles = (
        'Festival Name',
        'Year',
        'City',
        'Country',
        'Headliners',
        'Start Date',
        'End Date',
        'Lineup'
    )

    festivals_list = ''

    with open(data_filename) as infile:
        festivals_list = json.load(infile)

    try:
        wks = table.worksheet(wks_title)
    except gspread.exceptions.WorksheetNotFound as e:
        wks = table.add_worksheet(wks_title, 10, 8)

    wks_content = wks.get_all_values()

    if (wks.row_count - len(wks_content)) < len(festivals_list):
        wks.resize(len(wks_content) + len(festivals_list) + 1)

    if wks_content:
        posted_events_titledates = []
        table_row_counter = len(wks_content) + 1

        for item in wks_content:
            posted_events_titledates.append(item[0] + ' ' + item[5].replace('.', '/'))

        for index, item in enumerate(festivals_list, 1):
            line = ''
            line += item.values()[0]['festival_name'] if 'festival_name' in item.values()[0] else ''
            line += ' '
            line += item.values()[0]['start_date'] if 'start_date' in item.values()[0] else ''

            name = item.values()[0]['festival_name'] if 'festival_name' in item.values()[0] else None
            if name:
                logger.info('Google API: %s/%s %s' % (str(index-1), str(len(festivals_list)), name))
                # print index-1

            if line not in posted_events_titledates and line.strip():
                while True:
                    try:
                        cell_list = wks.range('A%s:H%s' % (table_row_counter, table_row_counter))
                    except (IOError, httplib.HTTPException) as e:
                        logger.warning(str(e))
                        # print e
                        time.sleep(2)
                        continue
                    except KeyboardInterrupt as e:
                        sys.exit(1)
                    except Exception as e:
                        # print e
                        credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                        gc = gspread.authorize(credentials)
                        table = gc.open_by_key(tablekey)
                        wks = table.worksheet(wks_title)
                        logger.warning(str(e))
                        time.sleep(2)
                        continue
                    break


                if item.values():
                    cell_list[0].value = item.values()[0]['festival_name'] if 'festival_name' in item.values()[0] else None
                    cell_list[1].value = item.values()[0]['year'] if 'year' in item.values()[0] else None
                    cell_list[2].value = item.values()[0]['city'] if 'city' in item.values()[0] else None
                    cell_list[3].value = item.values()[0]['country'] if 'country' in item.values()[0] else None
                    cell_list[4].value = ', '.join(item.values()[0]['headliners']) if 'headliners' in item.values()[0] else None
                    cell_list[5].value = item.values()[0]['start_date'] if 'start_date' in item.values()[0] else None
                    cell_list[6].value = item.values()[0]['end_date'] if 'end_date' in item.values()[0] else None
                    cell_list[7].value = ', '.join(item.values()[0]['lineup']) if 'lineup' in item.values()[0] else None

                while True:
                    try:
                        wks.update_cells(cell_list)
                        table_row_counter += 1
                    # except gspread.GSpreadExceptions as e:
                    except gspread.exceptions.HTTPError as e:
                        logger.warning(str(e))
                        # print e
                        credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                        gc = gspread.authorize(credentials)
                        table = gc.open_by_key(tablekey)
                        wks = table.worksheet(wks_title)
                        # print e
                        time.sleep(2)
                        continue
                    except KeyboardInterrupt as e:
                        sys.exit(1)
                    except Exception as e:
                        logger.warning(str(e))
                        # print e
                        continue
                    break


    else:
        while True:
            try:
                cell_list = wks.range('A1:H1')
            except (IOError, httplib.HTTPException) as e:
                logger.warning(str(e))
                # print e
                time.sleep(2)
                continue
            except KeyboardInterrupt as e:
                sys.exit(1)
            except Exception as e:
                logger.warning(str(e))
                credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                gc = gspread.authorize(credentials)
                table = gc.open_by_key(tablekey)
                wks = table.worksheet(wks_title)
                # print e
                time.sleep(2)
                continue
            break

        for index, cell in enumerate(cell_list, 1):
            cell.value = cells_titles[index-1]

        while True:
            try:
                wks.update_cells(cell_list)
            # except gspread.GSpreadExceptions as e:
            except gspread.exceptions.HTTPError as e:
                logger.warning(str(e))
                # print e
                credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                gc = gspread.authorize(credentials)
                table = gc.open_by_key(tablekey)
                wks = table.worksheet(wks_title)
                time.sleep(2)
                continue
            except KeyboardInterrupt as e:
                sys.exit(1)
            except Exception as e:
                logger.warning(str(e))
                # print e
                continue
            break


        for index, item in enumerate(festivals_list, 1):
            # print 'A%s:H%s' % (index+1, index+1)
            while True:
                try:
                    cell_list = wks.range('A%s:H%s' % (index+1, index+1))
                except (IOError, httplib.HTTPException) as e:
                    logger.warning(str(e))
                    # print e
                    time.sleep(2)
                    continue
                except KeyboardInterrupt as e:
                    sys.exit(1)
                except Exception as e:
                    logger.warning(str(e))
                    credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                    gc = gspread.authorize(credentials)
                    table = gc.open_by_key(tablekey)
                    wks = table.worksheet(wks_title)
                    # print e
                    time.sleep(2)
                    continue
                break

            if item.values():
                cell_list[0].value = item.values()[0]['festival_name'] if 'festival_name' in item.values()[0] else None
                cell_list[1].value = item.values()[0]['year'] if 'year' in item.values()[0] else None
                cell_list[2].value = item.values()[0]['city'] if 'city' in item.values()[0] else None
                cell_list[3].value = item.values()[0]['country'] if 'country' in item.values()[0] else None
                cell_list[4].value = ', '.join(item.values()[0]['headliners']) if 'headliners' in item.values()[0] else None
                cell_list[5].value = item.values()[0]['start_date'] if 'start_date' in item.values()[0] else None
                cell_list[6].value = item.values()[0]['end_date'] if 'end_date' in item.values()[0] else None
                cell_list[7].value = ', '.join(item.values()[0]['lineup']) if 'lineup' in item.values()[0] else None

            while True:
                try:
                    wks.update_cells(cell_list)
                except gspread.exceptions.HTTPError as e:
                    logger.warning(str(e))
                    # print e
                    credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                    gc = gspread.authorize(credentials)
                    table = gc.open_by_key(tablekey)
                    wks = table.worksheet(wks_title)
                    time.sleep(2)
                    continue
                except KeyboardInterrupt as e:
                    sys.exit(1)
                except Exception as e:
                    logger.warning(str(e))
                    # print e
                    continue
                break

def upload_list_to_google(results_list, cred_filename, tablekey):
    json_key = json.load(open(cred_filename))
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
    gc = gspread.authorize(credentials)

    table = gc.open_by_key(tablekey)
    wks_title = 'festivals'
    cells_titles = (
        'Festival Name',
        'Year',
        'City',
        'Country',
        'Headliners',
        'Start Date',
        'End Date',
        'Lineup'
    )

    festivals_list = results_list

    # with open(data_filename) as infile:
    #     festivals_list = json.load(infile)

    try:
        wks = table.worksheet(wks_title)
    except gspread.exceptions.WorksheetNotFound as e:
        wks = table.add_worksheet(wks_title, 10, 8)

    wks_content = wks.get_all_values()

    if (wks.row_count - len(wks_content)) < len(festivals_list):
        wks.resize(len(wks_content) + len(festivals_list) + 1)

    # wks.resize(len(wks_content) + len(festivals_list) + 1)

    if wks_content:
        posted_events_titledates = []
        table_row_counter = len(wks_content) + 1

        for item in wks_content:
            posted_events_titledates.append(item[0] + ' ' + item[5].replace('.', '/'))

        for index, item in enumerate(festivals_list, 1):
            line = ''
            line += item.values()[0]['festival_name'] if 'festival_name' in item.values()[0] else ''
            line += ' '
            line += item.values()[0]['start_date'] if 'start_date' in item.values()[0] else ''

            name = item.values()[0]['festival_name'] if 'festival_name' in item.values()[0] else None
            if name:
                logger.info('Google API: %s/%s %s' % (str(index-1), str(len(festivals_list)), name))
                # print index-1

            if line not in posted_events_titledates and line.strip():
                while True:
                    try:
                        cell_list = wks.range('A%s:H%s' % (table_row_counter, table_row_counter))
                    except (IOError, httplib.HTTPException) as e:
                        logger.warning(str(e))
                        # print e
                        time.sleep(2)
                        continue
                    except KeyboardInterrupt as e:
                        sys.exit(1)
                    except Exception as e:
                        logger.warning(str(e))
                        credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                        gc = gspread.authorize(credentials)
                        table = gc.open_by_key(tablekey)
                        wks = table.worksheet(wks_title)
                        # print e
                        time.sleep(2)
                        continue
                    break


                if item.values():
                    cell_list[0].value = item.values()[0]['festival_name'] if 'festival_name' in item.values()[0] else None
                    cell_list[1].value = item.values()[0]['year'] if 'year' in item.values()[0] else None
                    cell_list[2].value = item.values()[0]['city'] if 'city' in item.values()[0] else None
                    cell_list[3].value = item.values()[0]['country'] if 'country' in item.values()[0] else None
                    cell_list[4].value = ', '.join(item.values()[0]['headliners']) if 'headliners' in item.values()[0] else None
                    cell_list[5].value = item.values()[0]['start_date'] if 'start_date' in item.values()[0] else None
                    cell_list[6].value = item.values()[0]['end_date'] if 'end_date' in item.values()[0] else None
                    cell_list[7].value = ', '.join(item.values()[0]['lineup']) if 'lineup' in item.values()[0] else None

                while True:
                    try:
                        wks.update_cells(cell_list)
                        table_row_counter += 1
                    # except gspread.GSpreadExceptions as e:
                    except gspread.exceptions.HTTPError as e:
                        logger.warning(str(e))
                        # print e
                        credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                        gc = gspread.authorize(credentials)
                        table = gc.open_by_key(tablekey)
                        wks = table.worksheet(wks_title)
                        # print e
                        time.sleep(2)
                        continue
                    except KeyboardInterrupt as e:
                        sys.exit(1)
                    except Exception as e:
                        logger.warning(str(e))
                        # print e
                        continue
                    break


    else:
        while True:
            try:
                cell_list = wks.range('A1:H1')
            except (IOError, httplib.HTTPException) as e:
                logger.warning(str(e))
                # print e
                time.sleep(2)
                continue
            except KeyboardInterrupt as e:
                sys.exit(1)
            except Exception as e:
                logger.warning(str(e))
                credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                gc = gspread.authorize(credentials)
                table = gc.open_by_key(tablekey)
                wks = table.worksheet(wks_title)
                # print e
                time.sleep(2)
                continue
            break

        for index, cell in enumerate(cell_list, 1):
            cell.value = cells_titles[index-1]

        while True:
            try:
                wks.update_cells(cell_list)
            # except gspread.GSpreadExceptions as e:
            except gspread.exceptions.HTTPError as e:
                logger.warning(str(e))
                # print e
                credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                gc = gspread.authorize(credentials)
                table = gc.open_by_key(tablekey)
                wks = table.worksheet(wks_title)
                time.sleep(2)
                continue
            except KeyboardInterrupt as e:
                sys.exit(1)
            except Exception as e:
                logger.warning(str(e))
                # print e
                continue
            break


        for index, item in enumerate(festivals_list, 1):
            # print 'A%s:H%s' % (index+1, index+1)
            while True:
                try:
                    cell_list = wks.range('A%s:H%s' % (index+1, index+1))
                except (IOError, httplib.HTTPException) as e:
                    logger.warning(str(e))
                    # print e
                    time.sleep(2)
                    continue
                except KeyboardInterrupt as e:
                    sys.exit(1)
                except Exception as e:
                    logger.warning(str(e))
                    credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                    gc = gspread.authorize(credentials)
                    table = gc.open_by_key(tablekey)
                    wks = table.worksheet(wks_title)
                    # print e
                    time.sleep(2)
                    continue
                break

            if item.values():
                cell_list[0].value = item.values()[0]['festival_name'] if 'festival_name' in item.values()[0] else None
                cell_list[1].value = item.values()[0]['year'] if 'year' in item.values()[0] else None
                cell_list[2].value = item.values()[0]['city'] if 'city' in item.values()[0] else None
                cell_list[3].value = item.values()[0]['country'] if 'country' in item.values()[0] else None
                cell_list[4].value = ', '.join(item.values()[0]['headliners']) if 'headliners' in item.values()[0] else None
                cell_list[5].value = item.values()[0]['start_date'] if 'start_date' in item.values()[0] else None
                cell_list[6].value = item.values()[0]['end_date'] if 'end_date' in item.values()[0] else None
                cell_list[7].value = ', '.join(item.values()[0]['lineup']) if 'lineup' in item.values()[0] else None

            while True:
                try:
                    wks.update_cells(cell_list)
                except gspread.exceptions.HTTPError as e:
                    logger.warning(str(e))
                    # print e
                    credentials = sjac(json_key['client_email'], json_key['private_key'], scope)
                    gc = gspread.authorize(credentials)
                    table = gc.open_by_key(tablekey)
                    wks = table.worksheet(wks_title)
                    time.sleep(2)
                    continue
                except KeyboardInterrupt as e:
                    sys.exit(1)
                except Exception as e:
                    logger.warning(str(e))
                    # print e
                    continue
                break
#00000000000

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Songkick festivals getter/uploader',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog="""
    songkick_manager -d /path/to/file.dict --credentials-file /path/to/credentials.json --table_key TABLE_KEY
    songkick_manager --credentials-file /path/to/credentials.json --upcoming --table_key TABLE_KEY
        """)

    parser.add_argument('-c', '--credentials-file', nargs=1, required=True, help='specify file with google sheets credentials', type=lambda x: is_valid_file(parser, x))
    parser.add_argument('-t', '--table-key', nargs=1, required=True, help='specify table key where to upload', type=str)
    parser.add_argument('-d', '--data-upload', nargs=1, help='specify file with data to upload', type=lambda x: is_valid_file(parser, x))
    parser.add_argument('-u', '--upcoming', help='get upcoming festivals', action='store_true')
    parser.add_argument('-p', '--past', help='get past festivals', action='store_true')

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    # print args

    if args.data_upload and (args.upcoming or args.past):
        parser.error('choose upload _or_ scraping')

    if args.data_upload:
        upload_json_to_google(args.data_upload[0], args.credentials_file[0], args.table_key[0])

    else:
        festival_ids = []
        results = []

        if not args.past and not args.upcoming:
            parser.error('choose past or upcoming type')

        if args.past:
            festival_ids += get_festival_ids()
        if args.upcoming:
            festival_ids += get_festival_ids(upcoming=True)

        logger.info('Starting to fetch event info')

        festival_ids = filter(None, festival_ids)

        for index, festival_id in enumerate(festival_ids):
            logger.info('Event info fetching: %s/%s id: %s' % (index, str(len(festival_ids)), festival_id))
            results.append({festival_id: get_festival_info(festival_id)})
            time.sleep(1 * random.uniform(1, 2))

        upload_list_to_google(results, args.credentials_file[0], args.table_key[0])

