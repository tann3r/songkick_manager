songkick_manager
==============

Usage:
```
songkick_manager.py [-h] -c CREDENTIALS_FILE -t TABLE_KEY
                           [-d DATA_UPLOAD] [-u] [-p]
```

About:
```
Songkick festivals getter/uploader
```

Options:
```

  -h, --help            show this help message and exit
  -c CREDENTIALS_FILE, --credentials-file CREDENTIALS_FILE
                        specify file with google sheets credentials
  -t TABLE_KEY, --table-key TABLE_KEY
                        specify table key where to upload
  -d DATA_UPLOAD, --data-upload DATA_UPLOAD
                        specify file with data to upload
  -u, --upcoming        get upcoming festivals
  -p, --past            get past festivals
```

Examples of use:
```
songkick_manager -d /path/to/file.dict --credentials-file /path/to/credentials.json --table_key TABLE_KEY
songkick_manager --credentials-file /path/to/credentials.json --upcoming --table_key TABLE_KEY
```

